import Nav from './Nav';
import AttendeesList from './AttendeesList';
import LocationForm from './LocationForm';
import ConferenceForm from './ConferenceForm';
import AttendeeSignUpForm from './AttendeeSignUpForm';
import {BrowserRouter, Routes, Route} from "react-router-dom"
import PresentationForm from './PresentationForm'
import MainPage from './MainPage'
// this is where jsx is (reacts version of js with html)
// this file is were we write the component. We need to pass data in from the top level
// which is in the index.js file

function App(props) {
  if (props.attendees === undefined) {
    return null;
  }
  return (
    <>
    {/* <div className = "container"> */}
      <BrowserRouter>
      <Nav />
        <Routes>
          <Route path="/locations/new" element = {<LocationForm />}></Route>
          <Route path="/conferences/new" element = {<ConferenceForm  />}></Route>
          <Route path="/attendees"  element = {<AttendeesList attendees={props.attendees} />}></Route>
          <Route path="/attendees/new" element = {<AttendeeSignUpForm />}></Route>
          <Route path="/presentations/new" element = {<PresentationForm />}></Route>
          <Route index element={<MainPage />} />
        </Routes>
      </BrowserRouter>
      {/* </div> */}
      </>
  );
}

export default App;



// for (let attendee of props.attendees) {
//   <tr>
//     <td>{ attendee.name }</td>
//     <td>{ attendee.conference }</td>
//   </tr>



// To get tlist list length of attendees to display
// if (props.attendees === undefined) {
//   return null;
// }
// return (
//   <div >
//     Number of attendees: {props.attendees.length}
//   </div>
// );

// This is what the table initially said:
        /* <table className = "table table-striped">
          <thead>
            <tr>
              <th>Name</th>
              <th>Conference</th>
            </tr>
          </thead>
          <tbody >
            {props.attendees.map(attendee => {
              return(
              <tr key = {attendee.href}>
                <td>{ attendee.name }</td>
                <td>{ attendee.conference }</td>
              </tr>
            );
          })}
          </tbody>
        </table> */
