// import React from 'react';
import React, { useEffect, useState } from 'react';

// We're creating a stateful component here. That means that it can have internal data saved in the component when it gets created

function LocationForm () {
    const [states, setStates] = useState([]);

    const [name, setName] = useState('');
    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
      }
    const [roomCount, setRoomCount] = useState('');
    const handleRoomCountChange = (event) => {
        const value = event.target.value;
        setRoomCount(value);
      }
    const [city, setCity] = useState('');
    const handleCityChange = (event) => {
        const value = event.target.value;
        setCity(value);
      }
    const [state, setState] = useState('');
    const handleStateChange = (event) => {
        const value = event.target.value;
        setState(value);
      }

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};
        data.room_count = roomCount;
        data.name = name;
        data.city = city;
        data.state = state;
        console.log(data);

        const locationUrl = 'http://localhost:8000/api/locations/';
        const fetchConfig = {
          method: "post",
          body: JSON.stringify(data),
          headers: {
            'Content-Type': 'application/json',
          },
        };

        const response = await fetch(locationUrl, fetchConfig);
        if (response.ok) {
          const newLocation = await response.json();
          console.log(newLocation);

          setName('');
          setRoomCount('');
          setCity('');
          setState('');
        }
      }



// for getting the drop down to work
      const fetchData = async () => {
        const stateUrl = 'http://localhost:8000/api/states/';

        const stateResponse = await fetch(stateUrl);

        if (stateResponse.ok) {
          const stateData = await stateResponse.json();
          setStates(stateData.states)

        //   const selectTag = document.getElementById('state');
        //   for (let state of data.states) {
        //     const option = document.createElement('option');
        //     option.value = state.abbreviation;
        //     option.innerHTML = state.name;
        //     selectTag.appendChild(option);
        //   }
        }

      }

      useEffect(() => {
        fetchData();
      }, []);



    return (
        <div className="row">
            <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
                <h1>Create a new location</h1>
                <form onSubmit={handleSubmit} id="create-location-form">
                <div className="form-floating mb-3">
                    <input onChange={handleNameChange} name = "name" placeholder="Name" required type="text" id="name" className="form-control" value={name}/>
                    <label htmlFor="name">Name</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={handleRoomCountChange}name = "room_count" placeholder="Room count" required type="number" id="room_count" className="form-control" value = {roomCount}/>
                    <label htmlFor="room_count">Room count</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={handleCityChange}name = "city" placeholder="City" required type="text" id="city" className="form-control" value = {city}/>
                    <label htmlFor="city">City</label>
                </div>
                <div className="mb-3">
                    <select onChange={handleStateChange} required name = "state" id="state" className="form-select" value ={state}>
                    <option value>Choose a state</option>
                    {states.map(state => {
                        return (
                        <option key={state.abbreviation} value={state.abbreviation}>
                            {state.name}
                        </option>
                        );
                    })}
                    </select>
                </div>
                <button className="btn btn-primary">Create</button>
                </form>
            </div>
            </div>
      </div>
    );
}

export default LocationForm;
