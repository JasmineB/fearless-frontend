import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';

// this page adds the React libraries, react and react-dom, imports that CSS file
// we improt the App function from the App.js file
// 'root' and 'root.render' is telling React where to take that fake HTML and put it
// the <App /> runs our function and whatever JSX the function returns gets put in there
const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();


// getting the attendees
async function loadAttendees() {
  const response = await fetch('http://localhost:8001/api/attendees/');
  if (response.ok) {
    const data = await response.json();
    console.log(data);
    root.render(
      <React.StrictMode>
        <App attendees={data.attendees} />
      </React.StrictMode>
    );

  } else {
    console.error(response);
  }
}
loadAttendees();

// attendees is a variable set to the value inside the curly braces. That variable becomes
// a property on the props parameter and gets passed into the function. attendees is a component
