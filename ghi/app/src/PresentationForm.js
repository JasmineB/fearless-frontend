
import React, { useEffect, useState } from 'react';

function PresentationForm () {
    // this one line of code below if for the dropdown
    const [conferences, setConferences] = useState([]);

    const [presenter_name, setPresenterName] = useState('');
    const handlePresenterNameChange = (event) => {
        const value = event.target.value;
        setPresenterName(value);
      }
    const [company_name, setCompanyName] = useState('');
    const handleCompanyNameChange = (event) => {
        const value = event.target.value;
        setCompanyName(value);
      }
    const [presenter_email, setPresenterEmail] = useState('');
    const handlePresenterEmailChange = (event) => {
        const value = event.target.value;
        setPresenterEmail(value);
      }
    const [title, setTitle] = useState('');
    const handleTitleChange = (event) => {
        const value = event.target.value;
        setTitle(value);
      }

    const [synopsis, setSynopsis] = useState('');
    const handleSynopsisChange = (event) => {
          const value = event.target.value;
          setSynopsis(value);
        }

    const [conference, setConference] = useState('');
    const handleConferenceChange = (event) => {
              const value = event.target.value;
              setConference(value);
            }



    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};
        data.presenter_name = presenter_name;
        data.company_name= company_name;
        data.presenter_email = presenter_email;
        data.title = title;
        data.synopsis = synopsis;
        console.log(data);

        const presentationUrl = `http://localhost:8000/api/conferences/${conference}/presentations/`;
        const fetchConfig = {
          method: "post",
          body: JSON.stringify(data),
          headers: {
            'Content-Type': 'application/json',
          },
        };

        const response = await fetch(presentationUrl, fetchConfig);
        if (response.ok) {
          const newPresentation = await response.json();
          console.log(newPresentation);

          setPresenterName('');
          setPresenterEmail('');
          setCompanyName('');
          setTitle('');
          setSynopsis('');
          setConference('');
        }
      }




// this code is for drop down
    const fetchData = async () => {
    const conferenceUrl = 'http://localhost:8000/api/conferences/';

    const conferenceResponse = await fetch(conferenceUrl);

    if (conferenceResponse.ok) {
      const conferenceData = await conferenceResponse.json();
      setConferences(conferenceData.conferences)

    }
    }
    useEffect(() => {
        fetchData();
      }, []);


    // This code shows the form
    // change label for to htmlFor
    // change class to className
    // add onChange to all the inputs and select tags that have name and add values

    return (
        <div className="row">
            <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
                <h1>Create a new Presentation</h1>
                <form onSubmit={handleSubmit} id="create-presentation-form">
                <div className="form-floating mb-3">
                    <input onChange={handlePresenterNameChange} value={presenter_name} name = "presenter_name" placeholder="Presenter_Name" required type="text" id="presenter_name" className="form-control"/>
                    <label htmlFor="presenter_name">Presenter Name</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={handlePresenterEmailChange} value={presenter_email} name = "presenter_email" placeholder="Start_date" required type="email" id="presenter_email" className="form-control"/>
                    <label htmlFor="presenter_email">Presenter Email</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={handleCompanyNameChange} value={company_name} name = "company_name" placeholder="Company_name" required type="text" id="company_name" className="form-control"/>
                    <label htmlFor="company_name">Company Name</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={handleTitleChange} value={title} name = "title" placeholder="title" required type="text" id="title" className="form-control"/>
                    <label htmlFor="title">Title</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={handleSynopsisChange} value={synopsis} name = "synopsis" placeholder="synopsis" required type="text" id="synopsis" className="form-control"/>
                    <label htmlFor="synopsis">Synopsis</label>
                </div>
                <div className="form-floating mb-3">
                    <select onChange={handleConferenceChange} value={conference} name = "conference" placeholder="Conference" id="conference" className="form-select">
                    <option value="">Choose a Conference</option>
                    {conferences.map(conference => {
                            return (
                            <option key={conference.id} value={conference.id}>
                                {conference.name}
                            </option>
                            );
                        })}

                    </select>
                </div>
                <button className="btn btn-primary">Create</button>
                </form>
            </div>
            </div>
      </div>
    );
}

export default PresentationForm;
