import React, { useEffect, useState } from 'react';

function ConferenceForm () {
    const [locations, setLocations] = useState([]);

    const [name, setName] = useState('');
    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
      }
    const [starts, setStarts] = useState('');
    const handleStartChange = (event) => {
        const value = event.target.value;
        setStarts(value);
      }
    const [ends, setEnds] = useState('');
    const handleEndChange = (event) => {
        const value = event.target.value;
        setEnds(value);
      }
    const [description, setDescription] = useState('');
    const handleDescriptionChange = (event) => {
        const value = event.target.value;
        setDescription(value);
      }

    const [max_presentations, setMaxPres] = useState('');
    const handleMaxPresChange = (event) => {
          const value = event.target.value;
          setMaxPres(value);
        }

    const [max_attendees, setMaxAtten] = useState('');
    const handleMaxAttenChange = (event) => {
            const value = event.target.value;
            setMaxAtten(value);
          }

    const [location, setLocation] = useState('');
    const handleLocationChange = (event) => {
              const value = event.target.value;
              setLocation(value);
            }



    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};
        data.name = name;
        data.starts = starts;
        data.ends = ends;
        data.description = description;
        data.max_presentations = max_presentations;
        data.max_attendees = max_attendees;
        data.location = location;
        console.log(data);

        const locationUrl = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
          method: "post",
          body: JSON.stringify(data),
          headers: {
            'Content-Type': 'application/json',
          },
        };

        const response = await fetch(locationUrl, fetchConfig);
        if (response.ok) {
          const newLocation = await response.json();
          console.log(newLocation);

          setName('');
          setStarts('');
          setEnds('');
          setDescription('');
          setMaxPres('');
          setMaxAtten('');
          setLocation('');
        }
      }





    const fetchData = async () => {
    const locationUrl = 'http://localhost:8000/api/locations/';

    const locationResponse = await fetch(locationUrl);

    if (locationResponse.ok) {
      const locationData = await locationResponse.json();
      setLocations(locationData.locations)

    }
    }
    useEffect(() => {
        fetchData();
      }, []);


    // This code shows the form
    return (
        <div className="my-5 container">
            <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
                <h1>Create a new Conference</h1>
                <form onSubmit={handleSubmit} id="create-conference-form">
                    <div className="form-floating mb-3">
                        <input  onChange={handleNameChange} name = "name" placeholder="Name" required type="text" id="name" className="form-control" value={name}/>
                        <label htmlFor="name">Name</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={handleStartChange} name = "starts" placeholder="Start_date" required type="date" id="starts" className="form-control" value={starts}/>
                        <label htmlFor="room_count">Start Date</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={handleEndChange} name = "ends" placeholder="End_date" required type="date" id="ends" className="form-control" value={ends}/>
                        <label htmlFor="room_count">End Date</label>
                    </div>
                    <div className="form-floating mb-3">
                        <textarea onChange={handleDescriptionChange} name = "description" placeholder="Description" required type="text" id="description" className="form-control" value={description}></textarea>
                        <label htmlFor="city">Description</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={handleMaxPresChange} name = "max_presentations" placeholder="Max_presentations" required type="number" id="max_presentations" className="form-control" value={max_presentations}/>
                        <label htmlFor="city">Number of Presentations</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={handleMaxAttenChange} name = "max_attendees" placeholder="Number_attendees" required type="number" id="max_attendees" className="form-control" value={max_attendees}/>
                        <label htmlFor="city">Number of Attendees</label>
                    </div>
                    <div className="form-floating mb-3">
                        <select onChange={handleLocationChange} required name = "location" placeholder="Location" id="location" className="form-select" value={location}>
                        <option value= "">Choose a Location</option>
                        {locations.map(location => {
                            return (
                            <option key={location.id} value={location.id}>
                                {location.name}
                            </option>
                            );
                        })}
                        </select>
                    </div>
                    <button className="btn btn-primary">Create</button>
                </form>
            </div>
            </div>
      </div>
    );
}

export default ConferenceForm;
