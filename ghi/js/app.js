function createCard(name, description, pictureUrl, starts, ends, locationName) {
    return `

    <div class="col-2">
    <div class = "shadow p-3 mb-5 bg-white rounded" >
      <div class="card" margin: 20px>
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <h6 class="card-subtitle mb-2 text-muted">${locationName}</h6>
          <p class="card-text">${description}</p>
          <div class="card-footer">
          ${starts} - ${ends}
        </div>
        </div>
        </div>
      </div>
      </div>

    `;
  }



// window.addEventListener('DOMContentLoaded', async () => {
//     // const url = 'http://localhost:8000/api/conferences/';
//     // const response = await fetch(url);
//     // console.log(response);

//     // const data = await response.json();
//     // console.log(data);

// // updated code to handel errors
//     const url = 'http://localhost:8000/api/conferences/';

//     try {
//       const response = await fetch(url);

//       if (!response.ok) {
//         // Figure out what to do when the response is bad
//       } else {
//         const data = await response.json();
// //  get the first conference out of the array
//         const conference = data.conferences[0];
//         console.log(conference);
//         /*
//         You can use a CSS query selector to get a reference to that tag.
//         Then, you set the innerHTML property of the element to set the
//         name of the conference to the content of that element.
//         */
//         const nameTag = document.querySelector('.card-title');
//         nameTag.innerHTML = conference.name;


//         // detail data to get the conference description
//         const detailUrl = `http://localhost:8000${conference.href}`;
//         const detailResponse = await fetch(detailUrl);
//         if (detailResponse.ok) {
//           const details = await detailResponse.json();
//           console.log(details);


//           const conference = details.conference
//
//          const nameTag1 = document.querySelector('.card-text');
//           nameTag1.innerHTML = conference.description;
//           console.log(details)
//           console.log(conference.description)

//           const imageTag = document.querySelector('.card-img-top');
//           imageTag.src = conference.location.picture_url;
//         }
//       }
//     } catch (e) {
//       // Figure out what to do if an error is raised
//     }
// });

window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try {
      const response = await fetch(url);
      if (!response.ok) {
        // Figure out what to do when the response is bad
        console.log("You got an error")
        console.log(response)
        throw new Error("You got an error")
      } else {
        const data = await response.json();

        for (let conference of data.conferences) {
          const detailUrl = `http://localhost:8000${conference.href}`;
          const detailResponse = await fetch(detailUrl);
          if (detailResponse.ok) {
            const details = await detailResponse.json();
            const title = details.conference.name;
            const description = details.conference.description;
            const pictureUrl = details.conference.location.picture_url;
            const starts = new Date(details.conference.starts);
            const startDateFormat = starts.toLocaleDateString();
            const ends = new Date(details.conference.ends);
            const endDateFormat = ends.toLocaleDateString();
            const locationName = details.conference.location.name

            const html = createCard(title, description, pictureUrl, startDateFormat, endDateFormat, locationName);
            // console.log(html);
            const column = document.querySelector('.row');
            column.innerHTML += html;
          }
        }

      }
    } catch (e) {
      // Figure out what to do if an error is raised
        console.log("error", e);
    }
    });

    // can write the word debugger in code to see what works
