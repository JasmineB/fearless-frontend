window.addEventListener('DOMContentLoaded', async () => {
// getting the location drop down to work 
    const url = 'http://localhost:8000/api/locations/';

    try{
        const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      console.log(data)
    //   the getElementById location needs to be the same from the model and also in the html, can see it in detail encoder too
      const selectTag = document.getElementById('location');
    //   the locations comes from the views where we define getting everything we want.
      for (let location of data.locations) {

        const option = document.createElement('option');
        // the location needs an Id b/c the conference needs the location to have an id to reference it.
        option.value=location.id
        option.innerHTML=location.name

        selectTag.appendChild(option)
        console.log(option)
      }
    }
        }
        catch (error){
            console.error('error', error);
        }
// submitting the conference data
    const formTag = document.getElementById('create-conference-form');
    formTag.addEventListener('submit', async (event) => {
    event.preventDefault();
    // console.log('need to submit the form data');
    const formData = new FormData(formTag);
    const json = JSON.stringify(Object.fromEntries(formData));
    // console.log(json);

    const conferencesUrl = 'http://localhost:8000/api/conferences/';
    const fetchConfig = {
      method: "post",
      body: json,
      headers: {
        'Content-Type': 'application/json',
      },
    };
    const response =  await fetch(conferencesUrl, fetchConfig);
    if (response.ok) {
      formTag.reset();
      const newConference = await response.json();
      console.log(newConference);
    }



  });


  });
