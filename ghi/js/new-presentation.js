window.addEventListener('DOMContentLoaded', async () => {
    // getting the location drop down to work
        const url = 'http://localhost:8000/api/conferences/';

        try{
            const response = await fetch(url);

        if (response.ok) {
          const data = await response.json();
          console.log(data)
        //   the getElementById location needs to be the same from the model and also in the html, can see it in detail encoder too
          const selectTag = document.getElementById('conference');
        //   the locations comes from the views where we define getting everything we want.
          for (let conference of data.conferences) {

            const option = document.createElement('option');
            // the location needs an Id b/c the conference needs the location to have an id to reference it.
            option.value=conference.id
            option.innerHTML=conference.name

            selectTag.appendChild(option)
            console.log(option)

          }
        }
            }
            catch (error){
                console.error('error', error);
            }
    // submitting the conference data
        const formTag = document.getElementById('create-presentation-form');
        formTag.addEventListener('submit', async (event) => {
        event.preventDefault();
        // console.log('need to submit the form data');
        const formData = new FormData(formTag);
        const json = JSON.stringify(Object.fromEntries(formData));
        console.log(json);
        // json is a json string with all dictionariy fields,
            const conferenceId = JSON.parse(json).conference
            // use backticks in the url b/c of the formated string. above we say the value is conference.id so this json will only have that id info
        const presentationsUrl = `http://localhost:8000/api/conferences/${conferenceId}/presentations/`;
        const fetchConfig = {
          method: "post",
          body: json,
          headers: {
            'Content-Type': 'application/json',
          },
        };
        const response =  await fetch(presentationsUrl, fetchConfig);
        if (response.ok) {
          formTag.reset();
          const newPresentation = await response.json();
          console.log(newPresentation);
        }



      });


      });
