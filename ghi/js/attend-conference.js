window.addEventListener('DOMContentLoaded', async () => {
    const selectTag = document.getElementById('conference');

    const url = 'http://localhost:8000/api/conferences/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();

      for (let conference of data.conferences) {
        const option = document.createElement('option');
        // the id for conference to link to the attendees is the conference.href not the actual id
        // you can see this by looking at the urls in insomnia. The attendee needs to be linked to a
        // specific conference href.
        option.value = conference.href;
        option.innerHTML = conference.name;
        selectTag.appendChild(option);
      }
          // Here, add the 'd-none' class to the loading icon
          const select = document.querySelector('#loading-conference-spinner')
          select.classList.add("d-none")
    // Here, remove the 'd-none' class from the select tag
    // const selectRemove = document.createElement('select')
    // selectRemove.className = "conference"
    selectTag.classList.remove("d-none")
    }

// create the form to make an attentee to add to the back-end
// Get the attendee form element by its id
    const formTag = document.getElementById('create-attendee-form');
    // Add an event handler for the submit event
    formTag.addEventListener('submit', async (event) => {
        // Prevent the default from happening
    event.preventDefault();
    // console.log('need to submit the form data');
    // Create a FormData object from the form
    const formData = new FormData(formTag);
    // Get a new object from the form data's entries
    const json = JSON.stringify(Object.fromEntries(formData));
    console.log(json);

    const attendeesUrl = "http://localhost:8001/api/attendees/";
    // Create options for the fetch
    const fetchConfig = {
      method: "post",
      body: json,
      headers: {
        'Content-Type': 'application/json',
      },
    };
    // Make the fetch using the await keyword to the URL
    const attendeeResponse =  await fetch(attendeesUrl, fetchConfig);
    if (attendeeResponse.ok) {

               // Here, add the 'd-none' class to the loading icon
               const formsuccess = document.getElementById("success-message")
               //   select.className = "conference"
                 formsuccess.classList.remove("d-none")
           // Here, remove the 'd-none' class from the select tag
           // const selectRemove = document.createElement('select')
           // selectRemove.className = "conference"
           formTag.classList.add("d-none")




      formTag.reset();
      const newAttendee = await attendeeResponse.json();
    //   console.log(newAttendee);



    }



  });














  });
